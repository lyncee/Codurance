﻿using System;
using System.Linq;
using CoduranceChat.Services.Models;
using System.Collections.Generic;
using CoduranceChat.Services.Interfaces;
using CoduranceChat.Shared.Extensions;
using CoduranceChat.Shared.Helpers;
using CoduranceChat.Shared.Wrappers;

namespace CoduranceChat.Services.Implementations
{
    public class UserService : IUserService
    {
        private static Dictionary<string, UserModel> _users = new Dictionary<string, UserModel>();

        private readonly IDateTimeWrapper _dateTimeWrapper;

        public UserService(IDateTimeWrapper dateTimeWrapper)
        {
            _dateTimeWrapper = dateTimeWrapper;
        }


        /// <summary>
        /// Init a new user in the storage
        /// </summary>
        /// <param name="userName">User name</param>
        public void Init(string userName)
        {
            if (!_users.ContainsKey(userName))
            {
                _users.Add(userName, new UserModel());
            }
        }

        /// <summary>
        /// Display aggregated messages of a specific user wall, including following messages
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns></returns>
        public IEnumerable<string> GetAggregatedMessages(string userName)
        {
            var result = new List<MessageDetailsModel>();

            // We add the user messages
            var userMessages = _users[userName].Messages.Select(x => new MessageDetailsModel
            {
                Created = x.Created,
                Message = x.Message,
                UserName = userName
            }).ToList();

            result.AddRange(userMessages);

            // We add the following messages
            var followings = _users[userName].Following;

            foreach (var following in followings)
            {
                // We add all the following messages created after the user started following.
                var messages = _users[following.UserName]
                                     .Messages
                                     .Where(x => x.Created > following.Created)
                                     .Select(x => new MessageDetailsModel
                                     {
                                         Created = x.Created,
                                         Message = x.Message,
                                         UserName = following.UserName
                                     })
                                     .ToList();

                result.AddRange(messages);
            }

            return result.OrderByDescending(x => x.Created)
                                            .Select(x => $"{x.UserName} - {x.Message} ({x.Created.Elapsed()})")
                                            .ToList();
        }


        /// <summary>
        /// Display messages of a specific wall
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns></returns>
        public IEnumerable<string> GetPersonalMessages(string userName)
        {
            return _users[userName].Messages.OrderByDescending(x => x.Created)
                                            .Select(x => $"{userName} - {x.Message} ({x.Created.Elapsed()})")
                                            .ToList();
        }

        /// <summary>
        /// Post a message
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="message">Message</param>
        public void Post(string userName, string message)
        {
            var model = new MessageModel
            {
                Created = _dateTimeWrapper.Now(),
                Message = message
            };
            _users[userName].Messages.Add(model);
        }

        /// <summary>
        /// Follow a specific user
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="followingName">Following name</param>
        public void Follow(string userName, string followingName)
        {
            if (_users[userName].Following.Select(x => x.UserName).Contains(followingName))
            {
                return;
            }

            var model = new FollowingModel
            {
                Created = _dateTimeWrapper.Now(),
                UserName = followingName
            };
            _users[userName].Following.Add(model);
        }


        /// <summary>
        /// Returns the user state
        /// </summary>
        /// <returns></returns>
        internal Dictionary<string, UserModel> GetUsers()
        {
            return _users;
        }

        /// <summary>
        /// Reset the users cache
        /// </summary>
        internal void ReInitUsers()
        {
            _users = new Dictionary<string, UserModel>();
        }
    }
}
