﻿using System;

namespace CoduranceChat.Services.Models
{
    public class MessageModel
    {
        public string Message { get; set; }

        public DateTime Created { get; set; }
    }
}
