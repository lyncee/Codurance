﻿using System.Collections.Generic;

namespace CoduranceChat.Services.Models
{
    public class UserModel
    {
        public UserModel()
        {
            Messages = new List<MessageModel>();
            Following = new List<FollowingModel>();
        }

        public List<MessageModel> Messages { get; set; }

        public List<FollowingModel> Following { get; set; }
    }
}
