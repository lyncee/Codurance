﻿using System;

namespace CoduranceChat.Services.Models
{
    public class FollowingModel
    {
        public string UserName { get; set; }

        public DateTime Created { get; set; }
    }
}
