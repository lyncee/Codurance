﻿using System.Collections.Generic;

namespace CoduranceChat.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Init a new user in the storage
        /// </summary>
        /// <param name="userName">User name</param>
        void Init(string userName);

        /// <summary>
        /// Display aggregated messages of a specific user wall, including following messages
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns></returns>
        IEnumerable<string> GetAggregatedMessages(string userName);

        /// <summary>
        /// Post a message
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="message">Message</param>
        void Post(string userName, string message);

        /// <summary>
        /// Follow a specific user
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="followerName">Follower name</param>
        void Follow(string userName, string followerName);

        /// <summary>
        /// Display messages of a specific wall
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns></returns>
        IEnumerable<string> GetPersonalMessages(string userName);
    }
}
