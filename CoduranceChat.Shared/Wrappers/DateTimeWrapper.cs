﻿using System;

namespace CoduranceChat.Shared.Wrappers
{
    public interface IDateTimeWrapper
    {
        DateTime Now();
    }

    public class DateTimeWrapper : IDateTimeWrapper
    {
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
