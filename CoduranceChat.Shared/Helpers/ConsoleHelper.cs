﻿using System;
using CoduranceChat.Shared.Wrappers;

namespace CoduranceChat.Shared.Helpers
{
    /// <summary>
    /// Interface providing methods for console
    /// </summary>
    public interface IConsoleHelper
    {
        /// <summary>
        /// Read a command from the console
        /// </summary>
        /// <returns></returns>
        string Read();

        /// <summary>
        /// Display a line in the console
        /// </summary>
        /// <param name="message">Message</param>
        void Display(string message);

        /// <summary>
        /// Display a command line in the console
        /// </summary>
        /// <param name="command"></param>
        void Command(string command);

        /// <summary>
        /// Display an exception message in the console
        /// </summary>
        /// <param name="ex">Exception</param>
        void Exception(Exception ex);
    }

    /// <summary>
    /// Concrete implementation of IConsoleHelper
    /// </summary>
    public class ConsoleHelper : IConsoleHelper
    {
        private readonly IDateTimeWrapper _dateTimeWrapper;

        public ConsoleHelper(IDateTimeWrapper dateTimeWrapper)
        {
            _dateTimeWrapper = dateTimeWrapper;
        }

        /// <summary>
        /// Read a command from the console
        /// </summary>
        /// <returns></returns>
        public string Read()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }

        /// <summary>
        /// Display a line in the console
        /// </summary>
        /// <param name="message"></param>
        public void Display(string message)
        {
            Write(message, ConsoleColor.Yellow);
        }

        /// <summary>
        /// Dispkay a command line in the console
        /// </summary>
        /// <param name="command"></param>
        public void Command(string command)
        {
            Write(command, ConsoleColor.Cyan);
        }

        /// <summary>
        /// Display an exception message in the console
        /// </summary>
        /// <param name="ex">Exception</param>
        public void Exception(Exception ex)
        {
            Write(ex.Message, ConsoleColor.Red);
        }

        /// <summary>
        /// Write a new line in the console
        /// </summary>
        /// <param name="text">The message to write</param>
        /// <param name="color">The color to use in the console</param>
        private void Write(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
        }
    }
}
