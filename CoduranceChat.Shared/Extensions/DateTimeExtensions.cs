﻿using System;

namespace CoduranceChat.Shared.Extensions
{
    public static class DateTimeExtensions
    {
        public static string Elapsed(this DateTime date)
        {
            var now = DateTime.Now;

            var intYears = now.Year - date.Year;
            var intMonths = now.Month - date.Month;
            var intDays = now.Day - date.Day;
            var intHours = now.Hour - date.Hour;
            var intMinutes = now.Minute - date.Minute;
            var intSeconds = now.Second - date.Second;

            if (intYears > 0) return $"{intYears} {(intYears == 1 ? "year" : "years")} ago";
            if (intMonths > 0) return $"{intMonths} {(intMonths == 1 ? "month" : "months")} ago";
            if (intDays > 0) return $"{intDays} {(intDays == 1 ? "day" : "days")} ago";
            if (intHours > 0) return $"{intHours} {(intHours == 1 ? "hour" : "hours")} ago";
            if (intMinutes > 0) return $"{intMinutes} {(intMinutes == 1 ? "minute" : "minutes")} ago";
            if (intSeconds > 0) return $"{intSeconds} {(intSeconds == 1 ? "second" : "seconds")} ago";
            return $"{date.ToShortTimeString()} ago";
        }
    }
}
