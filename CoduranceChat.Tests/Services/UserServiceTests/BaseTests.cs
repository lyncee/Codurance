﻿using System.Collections.Generic;
using CoduranceChat.Services.Implementations;
using CoduranceChat.Services.Models;
using CoduranceChat.Shared.Wrappers;
using Moq;
using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    public class BaseTests
    {
        public Mock<IDateTimeWrapper> DateTimeWrapperMock;
        public UserService ServiceUnderTests;

        [SetUp]
        public void Setup()
        {
            DateTimeWrapperMock = new Mock<IDateTimeWrapper>();
        }

        [TearDown]
        public void TearDown()
        {
            ServiceUnderTests.ReInitUsers();
        }

        public void Inject()
        {
            ServiceUnderTests = new UserService(DateTimeWrapperMock.Object);

        }

        public void Init(string userName)
        {
            ServiceUnderTests.Init(userName);
        }

        public void Post(string userName, string message)
        {
            ServiceUnderTests.Post(userName, message);
        }

        public void Follow(string userName, string followingName)
        {
            ServiceUnderTests.Follow(userName, followingName);
        }

        public IEnumerable<string> GetPersonalMessages(string userName)
        {
            return ServiceUnderTests.GetPersonalMessages(userName);
        }

        public IEnumerable<string> GetAggregatedMessages(string userName)
        {
            return ServiceUnderTests.GetAggregatedMessages(userName);
        }

        public Dictionary<string, UserModel> GetUsers()
        {
            return ServiceUnderTests.GetUsers();
        }
    }
}
