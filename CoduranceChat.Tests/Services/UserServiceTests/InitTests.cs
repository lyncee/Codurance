﻿using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class InitTests : BaseTests
    {
        [Test]
        public void ensure_successfully_init_user_when_user_state_not_created()
        {
            // Arrange
            var userName = "guillaume";

            Inject();

            // Act
            Init(userName);

            // Assert
            var userState = ServiceUnderTests.GetUsers();
            Assert.AreEqual(1, userState.Count);
            var user = userState[userName];
            Assert.IsNotNull(user);
        }

        [Test]
        public void ensure_do_not_create_duplicates_when_user_already_created()
        {
            // Arrange
            var userName = "guillaume";

            Inject();

            // Act
            Init(userName);
            Init(userName);

            // Assert
            var userState = GetUsers();
            Assert.AreEqual(1, userState.Count);
            var user = userState[userName];
            Assert.IsNotNull(user);
        }
    }
}
