﻿using System;
using System.Linq;
using CoduranceChat.Shared.Extensions;
using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class GetPersonalMessagesTests : BaseTests
    {
        [Test]
        public void ensure_successfully_get_personal_messages()
        {
            // Arrange
            var userName = "guillaume";
            var message1 = "Hello";
            var message2 = "How are you?";
            var now = DateTime.Now;
            var expectedMessage1 = $"{userName} - {message1} ({now.Elapsed()})";
            var expectedMessage2 = $"{userName} - {message2} ({now.Elapsed()})";

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(now);

            Inject();

            Init(userName);

            Post(userName, message1);

            Post(userName, message2);

            // Act

            var messages = GetPersonalMessages(userName).ToList();

            // Assert
            var userState = GetUsers();
            Assert.AreEqual(1, userState.Count);
            var user = userState[userName];
            Assert.IsNotNull(user);
            Assert.AreEqual(2, messages.Count());
            Assert.Contains(expectedMessage1, messages);
            Assert.Contains(expectedMessage2, messages);
        }
    }
}
