﻿using System;
using System.Linq;
using CoduranceChat.Shared.Extensions;
using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    public class GetAggregatedMessagesTests: BaseTests
    {
        [Test]
        public void ensure_successfully_get_aggregated_messages()
        {
            // Arrange
            var userName1 = "guillaume";
            var userName2 = "alex";
            var message1 = "Hello";
            var message2 = "How are you?";
            var message3 = "Not bad!";
            var created1 = DateTime.Now.AddMinutes(-2);
            var created2 = DateTime.Now.AddMinutes(-1);
            var followed = DateTime.Now.AddSeconds(-30);
            var created3 = DateTime.Now;

            var expectedMessage1 = $"{userName1} - {message1} ({created1.Elapsed()})";
            var expectedMessage3 = $"{userName2} - {message3} ({created3.Elapsed()})";

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(created1);
            Inject();

            Init(userName1);

            Init(userName2);

            Post(userName1, message1);

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(created2);
            Inject();

            Post(userName2, message2);

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(followed);
            Inject();

            Follow(userName1, userName2);

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(created3);
            Inject();

            Post(userName2, message3);

            // Act
            var messages = GetAggregatedMessages(userName1).ToList();

            // Assert
            var userState = GetUsers();
            Assert.AreEqual(2, userState.Count);
            var user1 = userState[userName1];
            Assert.IsNotNull(user1);
            var user2 = userState[userName2];
            Assert.IsNotNull(user2);
            // We should only retrieve message1 and message3, as message2 was created before user1 started following user2
            Assert.AreEqual(1, user1.Messages.Count);
            Assert.AreEqual(2, user2.Messages.Count);
            Assert.AreEqual(2, messages.Count());
            Assert.Contains(expectedMessage1, messages);
            Assert.Contains(expectedMessage3, messages);
        }
    }
}
