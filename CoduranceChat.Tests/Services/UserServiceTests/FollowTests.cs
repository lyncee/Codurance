﻿using System;
using System.Linq;
using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class FollowTests : BaseTests
    {
        [Test]
        public void ensure_successfully_follow_user()
        {
            // Arrange
            var userName = "guillaume";
            var followingName = "alex";
            var now = DateTime.Now;

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(now);

            Inject();

            Init(userName);
            //TODO: Note that we don't init the following, but we should if the validation was done properly and was checking that both actors exist.

            // Act
            Follow(userName, followingName);

            // Assert
            var userState = GetUsers();
            Assert.AreEqual(1, userState.Count);
            var user = userState[userName];
            Assert.IsNotNull(user);
            Assert.That(user.Following.Any(x => x.Created == now && x.UserName == followingName));
        }
    }
}
