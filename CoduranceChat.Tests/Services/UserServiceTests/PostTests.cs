﻿using System;
using System.Linq;
using CoduranceChat.Services.Models;
using NUnit.Framework;

namespace CoduranceChat.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class PostTests : BaseTests
    {
        [Test]
        public void ensure_successfully_post_message()
        {
            // Arrange
            var userName = "guillaume";
            var message = "Hello, how are you ?";
            var now = DateTime.Now;

            var expectedStoredMessage = new MessageModel
            {
                Created = now,
                Message = message
            };

            DateTimeWrapperMock.Setup(x => x.Now()).Returns(now);

            Inject();

            Init(userName);

            // Act
            Post(userName, message);

            // Assert
            var userState = GetUsers();
            Assert.AreEqual(1, userState.Count);
            var user = userState[userName];
            Assert.IsNotNull(user);
            Assert.That(user.Messages.Any(x => x.Created == expectedStoredMessage.Created && x.Message == expectedStoredMessage.Message));
        }
    }
}
