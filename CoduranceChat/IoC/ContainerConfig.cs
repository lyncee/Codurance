﻿using Autofac;
using CoduranceChat.Services.Implementations;
using CoduranceChat.Services.Interfaces;
using CoduranceChat.Shared.Helpers;
using CoduranceChat.Shared.Wrappers;

namespace CoduranceChat.IoC
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<ConsoleHelper>().As<IConsoleHelper>();
            builder.RegisterType<DateTimeWrapper>().As<IDateTimeWrapper>();

            return builder.Build();
        }
    }
}
