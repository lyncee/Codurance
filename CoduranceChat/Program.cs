﻿using System;
using CoduranceChat.IoC;
using Autofac;
using CoduranceChat.Services.Interfaces;
using CoduranceChat.Shared.Helpers;
using CoduranceChat.Shared.Extensions;

namespace CoduranceChat
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<IUserService>();
                var consoleHelper = scope.Resolve<IConsoleHelper>();
                var quit = false;

                while (!quit)
                {
                    var command = consoleHelper.Read();

                    if (command == "quit")
                    {
                        consoleHelper.Command("Execute << Quit >>");
                        Console.ReadLine();
                        break;
                    }

                    var input = command.Split(' ');
                    var userName = input[0];

                    // Each time we receive a command from a new user, we need to add its state in our list of users.
                    userService.Init(userName);

                    // If there is only an username, we know it is a reading command
                    if (input.Length == 1)
                    {
                        consoleHelper.Command($"Execute << GetPersonalMessages({userName}) >>");
                        var messages = userService.GetPersonalMessages(userName);
                        foreach (var message in messages)
                        {
                            consoleHelper.Display(message);
                        }
                    }
                    else
                    {
                        // Otherwise, we check the second argument to figure which command
                        switch (input[1])
                        {
                            case "->":
                                {
                                    var fullMessage = string.Join(" ", input.SubArray(2, input.Length - 2));
                                    consoleHelper.Command($"Execute << Post({userName},{fullMessage}) >>");
                                    userService.Post(userName, fullMessage);
                                }
                                break;
                            case "follows":
                                {
                                    consoleHelper.Command($"Execute << Follow({userName},{input[2]}) >>");
                                    userService.Follow(userName, input[2]);
                                }
                                break;
                            case "wall":
                                {
                                    consoleHelper.Command($"Execute << GetAggregatedMessages({userName}) >>");
                                    var messages = userService.GetAggregatedMessages(userName);
                                    foreach (var message in messages)
                                    {
                                        consoleHelper.Display(message);
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
}
